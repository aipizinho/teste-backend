# SquadBR

## Introdução

Este repositório contém o código fonte do teste para novos programadores backend da Squad.

## Instalação

1. Clone o repositório:
```console
# Via HTTPS
git clone https://bitbucket.org/aipizinho/teste-backend.git

# Via SSH
git clone git@bitbucket.org:aipizinho/teste-backend.git
```

2. Depois de clonado, faça o `build` dos containers (isso pode levar alguns minutos):
```console
docker-compose -f ./local.yml build
```

3. Aplique os arquivos de migração:
```console
docker-compose -f ./local.yml run django python manage.py migrate
```

4. É possível criar dados ficticios com alguns estados e cidades para um teste manual da API, para isso rode:
```console
docker-compose -f ./local.yml run django python manage.py criar_dados_fake
```

5. Suba o docker:
```console
docker-compose -f ./local.yml up
```

## Acessando a API

Para o projeto, 3 endpoints foram criado:

- http://localhost:8000/api/v1/cidades
- http://localhost:8000/api/v1/estados
- http://localhost:8000/api/v1/docs


## Rodando os testes

Para rodar a bateria de testes basta rodar o seguinte comando:
```console
docker-compose -f ./local.yml run django py.test
```
