from rest_framework import serializers
from squadbr.core.models import Cidade, Estado


class CidadeSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(
        view_name='api:cidade-detail',
        lookup_field='pk'
    )

    class Meta:
        fields = ('url', 'nome', 'estado',)
        lookup_field = 'nome'
        model = Cidade


class EstadoSerializer(serializers.ModelSerializer):
    nome = serializers.SerializerMethodField()
    url = serializers.HyperlinkedIdentityField(
        view_name='api:estado-detail',
        lookup_field='uf'
    )
    # cidades_set = CidadeSerializer(source='cidades', many=True)

    class Meta:
        fields = ('url', 'nome', 'uf',)  # , 'cidades_set' )
        lookup_field = 'uf'
        model = Estado

    def get_nome(self, obj):
        return obj.get_uf_display()
