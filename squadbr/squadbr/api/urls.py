from django.urls import include, path
from rest_framework.routers import DefaultRouter

from squadbr.api.views import (EstadoViewSet, CidadeViewSet)

v1router = DefaultRouter()

app_name = "api"

v1router.register(
    r'cidades',
    CidadeViewSet,
    'cidade'
)
v1router.register(
    r'estados',
    EstadoViewSet,
    'estado'
)

urlpatterns = [
    path('', include(v1router.urls)),
]
