from itertools import cycle
from django.urls import reverse
from model_mommy import mommy
from rest_framework import status
from rest_framework.test import APITestCase

from squadbr.core.models import Cidade, Estado


class ParticipanteTests(APITestCase):

    def setUp(self):
        self.estados = ['SP', 'AC', 'RJ', 'AM']
        mommy.make(
            Cidade, estado__uf=cycle(self.estados),
            _quantity=len(self.estados)
        )

    def test_detail(self):
        cidade = Cidade.objects.first()
        url = reverse('api:cidade-detail', args=[cidade.pk])
        request = self.client.get(url)
        self.assertEqual(request.status_code, status.HTTP_200_OK)
        self.assertEqual(request.data['nome'], cidade.nome)
        self.assertEqual(request.data['estado'], cidade.estado.pk)

    def test_filter_city(self):
        estado = Estado.objects.first()
        nome_cidade = 'ABCD'
        Cidade.objects.create(estado=estado, nome=nome_cidade)
        url = reverse('api:cidade-list')
        request = self.client.get(url, {'nome': nome_cidade})
        self.assertEqual(request.status_code, status.HTTP_200_OK)
        self.assertEqual(len(dict(request.data)['results']), 1)
        self.assertEqual(
            dict(dict(request.data)['results'][0])['nome'],
            nome_cidade
        )
        self.assertEqual(
            dict(dict(request.data)['results'][0])['estado'],
            estado.pk
        )

    def test_filter_state(self):
        estados = Estado.objects.all()[0:2]
        nome_cidade = 'EFGH'
        mommy.make(
            Cidade,
            estado=cycle(estados),
            nome=nome_cidade,
            _quantity=len(estados)
        )
        url = reverse('api:cidade-list')
        request = self.client.get(
            url,
            {'estado__uf': estados[0].uf}
        )
        self.assertEqual(request.status_code, status.HTTP_200_OK)
        self.assertEqual(dict(request.data)['count'], 2)

    def test_filter_city_with_state(self):
        estados = Estado.objects.all()[0:2]
        nome_cidade = 'EFGH'
        mommy.make(
            Cidade,
            estado=cycle(estados),
            nome=nome_cidade,
            _quantity=len(estados)
        )
        url = reverse('api:cidade-list')
        request = self.client.get(
            url,
            {'nome': nome_cidade, 'estado__uf': estados[0].uf}
        )
        self.assertEqual(request.status_code, status.HTTP_200_OK)
        self.assertEqual(dict(request.data)['count'], 1)
        self.assertEqual(
            dict(dict(request.data)['results'][0])['nome'],
            nome_cidade
        )
        self.assertEqual(
            dict(dict(request.data)['results'][0])['estado'],
            estados[0].pk
        )

    def test_filter_treatment_character(self):
        estado = Estado.objects.first()
        url = reverse('api:cidade-list')
        nome_cidade = 'XPTO'
        Cidade.objects.create(estado=estado, nome=nome_cidade)
        request = self.client.get(
            url,
            {'estado__uf': estado.uf, 'nome': nome_cidade + ']'}
        )
        self.assertEqual(request.status_code, status.HTTP_200_OK)
        self.assertEqual(dict(request.data)['count'], 1)
        self.assertEqual(
            dict(dict(request.data)['results'][0])['nome'],
            nome_cidade
        )
        self.assertEqual(
            dict(dict(request.data)['results'][0])['estado'],
            estado.pk
        )

    def test_get(self):
        url = reverse('api:cidade-list',)
        request = self.client.get(url)
        self.assertEqual(request.status_code, status.HTTP_200_OK)
        self.assertEqual(len(request.data), len(self.estados))

    def test_post(self):
        estado = Estado.objects.first()
        url = reverse('api:cidade-list')
        nome_cidade = 'XPTO'
        response = self.client.post(
            url,
            {'estado': estado.pk, 'nome': nome_cidade}
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(Cidade.objects.get(estado=estado, nome=nome_cidade))

    def test_post_twice_same_city_in_same_state(self):
        estado = Estado.objects.first()
        url = reverse('api:cidade-list')
        nome_cidade = 'XPTO'
        response = self.client.post(
            url,
            {'estado': estado.pk, 'nome': nome_cidade}
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(Cidade.objects.get(estado=estado, nome=nome_cidade))
        response = self.client.post(
            url,
            {'estado': estado.pk, 'nome': nome_cidade}
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
