from itertools import cycle
from django.urls import reverse
from model_mommy import mommy
from rest_framework import status
from rest_framework.test import APITestCase

from squadbr.core.models import Estado


class ParticipanteTests(APITestCase):

    def setUp(self):
        self.estados = ['SP', 'AC', 'RJ', 'AM']
        mommy.make(
            Estado, uf=cycle(self.estados),
            _quantity=len(self.estados)
        )

    def test_detail(self):
        estado = Estado.objects.first()
        url = reverse('api:estado-detail', args=[estado.uf])
        request = self.client.get(url)
        self.assertEqual(request.status_code, status.HTTP_200_OK)
        self.assertEqual(request.data['uf'], estado.uf)

    def test_get(self):
        url = reverse('api:estado-list',)
        request = self.client.get(url)
        self.assertEqual(request.status_code, status.HTTP_200_OK)
        self.assertEqual(len(request.data), len(self.estados))

    def test_post(self):
        url = reverse('api:estado-list')
        uf_estado = 'AL'
        response = self.client.post(
            url,
            {'uf': uf_estado}
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(Estado.objects.get(uf=uf_estado))

    def test_post_twice_same_estado(self):
        url = reverse('api:estado-list')
        uf_estado = 'SP'
        response = self.client.post(
            url,
            {'uf': uf_estado}
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
