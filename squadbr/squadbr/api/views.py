import re

from django.core.validators import EMPTY_VALUES
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets

from squadbr.core.models import Cidade, Estado
from squadbr.core.utils.regex import Regex
from .serializers import CidadeSerializer, EstadoSerializer

regex = Regex()


class EstadoViewSet(viewsets.ModelViewSet):
    queryset = Estado.objects.all()
    serializer_class = EstadoSerializer
    lookup_field = 'uf'
    lookup_value_regex = (regex.uf)


class Teste(DjangoFilterBackend):
    def filter_queryset(self, request, queryset, view):
        nome = request.query_params.get('nome')
        estado = request.query_params.get('estado__uf')

        if nome:
            alpha_chars = re.compile('[^a-zA-Z\u00C0-\u00FF ]')
            nome = alpha_chars.sub('', nome)

        if nome not in EMPTY_VALUES and estado not in EMPTY_VALUES:
            queryset = queryset.filter(estado__uf=estado, nome=nome)

        if nome not in EMPTY_VALUES and estado in EMPTY_VALUES:
            queryset = queryset.filter(nome=nome)

        if nome in EMPTY_VALUES and estado not in EMPTY_VALUES:
            queryset = queryset.filter(estado__uf=estado)

        return queryset


class CidadeViewSet(viewsets.ModelViewSet):
    queryset = Cidade.objects.all()
    serializer_class = CidadeSerializer
    lookup_field = 'pk'
    filter_backends = (Teste,)
    filter_fields = ('estado__uf', 'nome')
