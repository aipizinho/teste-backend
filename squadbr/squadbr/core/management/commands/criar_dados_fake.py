from django.core import management

from squadbr.core.utils.criar_dados import *


class Command(management.BaseCommand):
    help = 'Criar dados fakes'

    def handle(self, *args, **options):
        management.call_command("flush")
        dados_fake = CriarDadosFicticios()
        dados_fake.criar_cidades()
