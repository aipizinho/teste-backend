from django.apps import AppConfig


class CoreConfig(AppConfig):
    name = 'squadbr.core'
    verbose_name = 'Core'
