from django.core.exceptions import ValidationError
from django.db import models
from django.db.models.query import QuerySet

from squadbr.core.utils.constants import ESTADOS


class SquadBRQuerySet(QuerySet):
    """Deal with errors when object does not exist

    Args:
        **kwargs: Receive attribute and the data to be seach for

    Returns:
        QuerySet: Return the objects if find, else return None
    """
    def get_or_none(self, **kwargs):
        try:
            return self.get(**kwargs)
        except self.model.DoesNotExist:
            return None


class CommonModel(models.Model):
    objects = SquadBRQuerySet.as_manager()

    def save(self, *args, **kwargs):
        self.full_clean()
        return super(CommonModel, self).save(*args, **kwargs)

    class Meta:
        abstract = True


class Cidade(CommonModel):
    nome = models.CharField(max_length=255)
    estado = models.ForeignKey(
        'Estado',
        models.PROTECT,
        related_name='cidades',
        related_query_name='cidade',
    )

    class Meta:
        ordering = ('nome', 'estado')
        verbose_name = 'Cidade'
        verbose_name_plural = 'Cidades'
        unique_together = ('nome', 'estado')

    def __str__(self):
        return '[%s - %s]' % (self.nome, self.estado.uf)


class Estado(CommonModel):
    uf = models.CharField(
        max_length=19,
        choices=ESTADOS,
        error_messages={
            'invalid_choice': '"%(value)s" não é um estado válido.'
        },
        unique=True,
    )

    class Meta:
        ordering = ('uf',)
        verbose_name = 'Estado'
        verbose_name_plural = 'Estados'

    def __str__(self):
        return '[%s - %s]' % (self.get_uf_display(), self.uf)
