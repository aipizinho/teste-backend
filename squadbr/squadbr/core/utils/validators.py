import re

from django.core.validators import RegexValidator

from .regex import Regex

regex = Regex()

INVALID_UF = (
    'UF inválido. UF deve conter somente 2 caracteres em letras maiúsculas.',
)

UF = re.compile(r'^{0}$'.format(regex.uf))


def validate_uf(value):
    validator = RegexValidator(
        regex=UF,
        message=INVALID_UF,
        code='invalid'
    )
    validator(value)
