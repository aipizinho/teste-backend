from django.core.exceptions import ValidationError
from django.test import TestCase
from model_mommy import mommy

from squadbr.core.models import Estado


class TestEstado(TestCase):
    """
    Test of Estado model
    """
    def setUp(self):
        self.uf = 'CE'
        self.estado = Estado.objects.create(uf=self.uf)

    def test__str__(self):
        """
        Checks if __str__ of the template returns as assigned pattern:
            [<get_uf_display> - <uf>]
        """
        display = 'Ceará'
        self.assertEqual(
            str(self.estado),
            '[{} - {}]'.format(
                display,
                self.uf,
            )
        )

    def test_invalid_uf(self):
        """
        Checks for the error message of the uf attribute if an invalid uf
            entry is persisted.
        """
        mesage = (
            '"{}" não é um estado válido.'
        )

        uf_list = ['SSP', 'sp', 's']

        for uf in uf_list:
            with self.assertRaisesMessage(ValidationError, mesage.format(uf)):
                mommy.make(
                    Estado,
                    uf=uf,
                )

    def test_meta_verbose_name(self):
        """
        Checks if Estado.Meta.verbose_name matches the class name.
        """
        self.assertEqual(
            str(Estado._meta.verbose_name), 'Estado')

    def test_meta_verbose_name_plural(self):
        """
        Checks if Estado.Meta.verbose_name_plural matches the plural class
            name.
        """
        self.assertEqual(str(Estado._meta.verbose_name_plural), 'Estados')

    def test_oneness(self):
        """
        Ensure the oneness of field state
        """
        Estado.objects.create(uf='PE')
        mesage = "{'uf': ['Estado with this Uf already exists.']}"
        with self.assertRaisesMessage(ValidationError, mesage):
            Estado.objects.create(uf='PE')
