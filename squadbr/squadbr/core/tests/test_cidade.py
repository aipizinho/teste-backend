from itertools import cycle

from django.test import TestCase
from model_mommy import mommy

from squadbr.core.models import Cidade, Estado


class TestCidade(TestCase):
    """
    Test of Cidade model
    """
    def setUp(self):
        self.nome = 'São Paulo'
        self.estado = mommy.make(Estado, uf='SP')
        self.cidade = Cidade.objects.create(estado=self.estado, nome=self.nome)

    def test__str__(self):
        """
        Checks if __str__ of the template returns as assigned pattern:
            [<nome> - <estado.uf>]
        """
        self.assertEqual(
            str(self.cidade),
            '[{} - {}]'.format(
                self.cidade.nome,
                self.estado.uf,
            )
        )

    def test_meta_verbose_name(self):
        """
        Checks if Cidade.Meta.verbose_name matches the class name.
        """
        self.assertEqual(
            str(Cidade._meta.verbose_name), 'Cidade')

    def test_meta_verbose_name_plural(self):
        """
        Checks if Cidade.Meta.verbose_name_plural matches the plural class
            name.
        """
        self.assertEqual(str(Cidade._meta.verbose_name_plural), 'Cidades')

    def test_ordering(self):
        """
        Checks whether the filter order matches the order in the template.
        """
        nomes_cidades = [
            'Bom Jesus',
            'Serra Talhada',
            'Juazeiro do Norte',
            'Manaus',
        ]
        ufs = ['RN', 'PE', 'CE', 'AM']
        cidades = mommy.make(
            Cidade,
            nome=cycle(nomes_cidades),
            estado__uf=cycle(ufs),
            _quantity=len(ufs)
        )

        qs = Cidade.objects.all()

        self.assertEqual(qs[0], cidades[0])
        self.assertEqual(qs[1], cidades[2])
        self.assertEqual(qs[2], cidades[3])
        self.assertEqual(qs[3], self.cidade)
        self.assertEqual(qs[4], cidades[1])

    def test_related_name_e_related_query_name_estado(self):
        """
        Ensure that the related_name and related_query_name in the return
            correct values.
        """
        cidade_2 = mommy.make(
            Cidade,
            nome='Osasco',
            estado=self.estado
        )

        qs = Estado.objects.get(cidade__nome='São Paulo')
        self.assertEqual(qs, self.estado)
        self.assertIn(self.cidade, qs.cidades.all())
        self.assertIn(cidade_2, qs.cidades.all())
